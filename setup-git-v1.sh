#!/bin/bash
#Created by Eric Dubois
#Spooned and modified by bluscrn

##################################################################################################################
# Author 	: 	Eric Dubois
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

# start ssh-agent so that ssh keys work with git
eval "$(ssh-agent -s)"

set -e

# change your name and email.
git config --global user.name "bluscrn"
git config --global user.email "bluscrn.signup@gmail.com"
sudo git config --system core.editor subl
# git config --global credential.helper cache
# git config --global credential.helper 'cache --timeout=25000'
git config --global push.default simple


echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"