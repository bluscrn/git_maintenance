#!/bin/bash
#Created by Eric Dubois
#Spooned and modified by bluscrn

# start ssh-agent so that ssh keys work with git
#eval "$(ssh-agent -s)"

# Below command will backup everything inside the project folder
# git add --all .

# Give a comment to the commit
echo "####################################"
echo "##   Write your commit comment!   ##"
echo "####################################"

read input

# Committing to the local repository with a message containing the time details and commit text

git commit -am "$input"

# checking if I have the latest files from master
echo "Checking for newer files online first"
git pull --rebase origin master

# Push the local files to a remote

git push -u origin master


echo "################################################################"
echo "###################    Git Push Done      ######################"
echo "################################################################"